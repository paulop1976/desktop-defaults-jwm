Modern looking (similar to MX Linux) configuration for JWM.
Details (with screen grabs)  in the forum: https://www.antixforum.com/forums/topic/making-jwm-desktop-look-more-modern-mx-linux-like/#post-22321

Remove contents of tray file
use the following as contents for .jwmr (note: it blocks the possibility of using themes, the way its right now, by design)

<?xml version="1.0"?>

<JWM>

<!-- Initialize the applications used in conjunction with JWM -->
<Include>$HOME/.jwm/startup</Include>

<!-- The root menu, if this is undefined you will not get a menu. -->
<!-- Additional RootMenu attributes: onroot, labeled, label -->
<RootMenu height="20">
	<!-- The backbone antiX menu -->
	<Include>$HOME/.jwm/menu</Include>
	<!-- Added to the end of the menu (more menu entries can be added) -->
   <Separator/>
   <Menu label="Terminar Sessão..." icon="/usr/share/icons/Faenza-Cupertino-mini/actions/32/application-exit.png">
     <Restart label="Reiniciar JWM" icon="/usr/share/icons/Faenza-Cupertino-mini/actions/32/view-refresh.png"/>
     <Program label="Terminar Sessão" icon="/usr/share/icons/Faenza-Cupertino-mini/actions/32/application-exit.png">desktop-session-exit</Program>
   </Menu>
</RootMenu>

<Include>$HOME/.jwm/keys</Include>
<Include>$HOME/.jwm/theme</Include>
<Include>$HOME/.jwm/tray</Include>
<Include>$HOME/.jwm/preferences</Include>

<!-- TRAY -->
<!-- Additional Tray attributes: autohide, width, border, layer, layout, valign -->
<Tray x="0" y="-1" height="28" layer="above" valign="bottom">
	<!-- Additional TrayButton attributes: label, icon, popup -->
	<!-- TRAY BUTTONS - INCLUDING THE MENU BUTTON, all icons to the left of the tasklist-->
		<TrayButton  label=" " popup="Menu" icon="/usr/share/icons/numix-bevel-antix/32x32/apps/windows.png">root:1</TrayButton>
		<Spacer width="5"/>
		<TrayButton label="" popup="Search Applications via keyboard (ESC exits, Enter selects app)" icon="/usr/share/icons/papirus-antix/32x32/apps/5961_Defunct_x86.0.png">exec:dmenu_run -l 10 -i -b -fn 10×20 -nb black -nf white -sb grey -sf black</TrayButton>
		<TrayButton label="" popup="Terminal" icon="/usr/share/icons/papirus-antix/32x32/apps/Terminal.png">exec:lxterminal</TrayButton>
		<TrayButton label="" popup="Space File Manager" icon="/usr/share/icons/papirus-antix/32x32/apps/system-file-manager.png">exec:spacefm</TrayButton>
		<TrayButton label="" popup="FireFox Browser" icon="/usr/share/icons/numix-bevel-antix/32x32/apps/firefox.png">exec:firefox</TrayButton>
		<TrayButton label="" popup="Desktop" icon="/usr/share/icons/papirus-antix/32x32/places/user-blue-desktop.png">showdesktop</TrayButton>
		<!-- The next line is for "skippy-xd"- a Expose like task switcher. Install the 60kb deb file from https://code.google.com/archive/p/skippy-xd/downloads or feel free to delete the next line if you do not want to use it -->	
		<TrayButton label="" popup="Task Switcher" icon="/usr/share/icons/papirus-antix/32x32/apps/cs-workspaces.png">exec:skippy-xd</TrayButton>
		<Spacer width="10"/>

<!-- TASK LIST - displays the names of all running windows. Choose maxwidth of 25 to see only the icons or 325 for viewing "normal" long windows names-->
<!-- Additional TaskList attribute: maxwidth -->
   <TaskList maxwidth="325"/>

   <!-- Additional Dock attribute: none -->
   <Dock/>
   <!-- Additional Swallow attribute: height, width -->
   <!-- <Swallow name="xload" height="28" width="30"> xload -update 2 -jumpscroll 1 -nolabel -bg black -fg green -hl black </Swallow> -->
	<!-- TRAY BUTTONS - icons to the right of the tasklist-  eject, connman-->
		<TrayButton label="" popup="Eject USB devices" icon="/usr/share/icons/numix-bevel-antix/32x32/actions/media-eject.png">exec:unplugdrive.sh</TrayButton>
		<TrayButton label="" popup="Network settings (Connman)" icon="/usr/share/icons/numix-bevel-antix/32x32/apps/cs-network.png">exec:connman-gtk</TrayButton>
   <!-- CLOCK -->
   <!-- Additional Clock attributes: format, height, width, zone -->
		<Clock format="%H:%M">
		<Text>white</Text>
		<Button mask="123">exec:yad --calendar --width=400 --no-buttons  --title=Calendar --mouse</Button></>
	<!-- TRAY BUTTONS - icons to the right of the clock- Exit-->
	<TrayButton label="" popup="Exit Session" icon="/usr/share/icons/papirus-antix/32x32/apps/xfsm-logout.png">exec:desktop-session-exit</TrayButton>

</Tray>
	
<!-- VISUAL STYLES- OR THEMES -->

<!-- WINDOW visual theme-->
	<WindowStyle>
		<Font>ubuntu-11:bold</Font>
		<Width>5</Width>
		<Height>26</Height>
		<Corner>4</Corner>
		<Foreground>#969696</Foreground>
		<Background>#555555</Background>
		<Outline>#000000</Outline>
		<Opacity>0.5</Opacity>
		<Active>
			<Foreground>#DDDDDD</Foreground>
			<Background>#333333:#AAAAAA</Background>
			<Outline>#333333</Outline>
			<Opacity>1.0</Opacity>
		</Active>
	</WindowStyle>

<!-- TRAY visual theme-->
	<TrayStyle group="false" list="all">
		<Font>ubuntu-10</Font>
		<Background>#555555</Background>
		<Foreground>white</Foreground>
		<Outline>#000000</Outline>
		<Opacity>0.75</Opacity>
	</TrayStyle>

<!-- CLOCK visual theme-->
<ClockStyle>
	<Font antialias="false">smoothansi</Font>
	<Font>ubuntu-10</ont>
	<Text>white</Text>
	<Foreground>white</Foreground>
	<Background>#555555</Background>
</ClockStyle>

<!-- UNUSED- PAGER visual theme-->
<PagerStyle>
		<Outline>#222222</Outline>
		<Foreground>#555555</Foreground>
		<Background>#333333</Background>
		<Text>#FFFFFF</Text>
		<Active>
			<Foreground>#0077CC</Foreground>
			<Background>#333333:#AAAAAA</Background>
		</Active>
	</PagerStyle>

<!-- MENU visual theme-->
	<MenuStyle>
		<Font>ubuntu-9</Font>
		<Foreground>#FFFFFF</Foreground>
		<Background>#333333:#FFFFFF</Background>
		<Outline>#222222</Outline>
		<Active>
			<Background>#0077CC</Background>
			<Foreground>white</Foreground>
			<Background>#333333:#AAAAAA</Background>
		</Active>
		<Opacity>0.85</Opacity>
	</MenuStyle>

<!-- POPUP DECORATIONS-->	
	<PopupStyle>
		<Font>ubuntu-9</Font>
		<Foreground>white</Foreground>
		<Background>#222222</Background>
		<Outline>#222222</Outline>
	</PopupStyle>

<!-- Virtual Desktops -->
	<!-- Desktop tags can be contained within Desktops for desktop names. -->
	<Desktops width="4" height="1">
		<!-- Default background. Note that a Background tag can be
			  contained within a Desktop tag to give a specific background
			  for that desktop.
		 -->
		<Background type="solid">#111111</Background>
	</Desktops>

<!-- shades of grey - some extra decorations from that theme-->
<WindowStyle>
	</Active>
		<Foreground>grey44</Foreground>
		<Background>grey22:black</Background>
	<Outline>black</Outline>
</WindowStyle>

<!-- Double click speed (in milliseconds) -->
	<DoubleClickSpeed>400</DoubleClickSpeed>

<!-- Double click delta (in pixels) -->
	<DoubleClickDelta>2</DoubleClickDelta>

<!-- The focus model (sloppy or click) -->
	<FocusModel>click</FocusModel>

<!-- The snap mode (none, screen, or border) -->
	<SnapMode distance="10">border</SnapMode>

<!-- The move mode (outline or opaque) and show/hide coordinates when moving a window -->
	<MoveMode>opaque</MoveMode>
	<MoveMode coordinates="off">outline</MoveMode>

<!-- The resize mode (outline or opaque) -->
	<ResizeMode>opaque</ResizeMode>

<!-- KEY BINDINGS - this definitions are used in conjunction with those in the .keys file -->
	<Key key="Up">up</Key>
	<Key key="Down">down</Key>
	<Key key="Right">right</Key>
	<Key key="Left">left</Key>
	<Key key="h">left</Key>
	<Key key="j">down</Key>
	<Key key="k">up</Key>
	<Key key="l">right</Key>
	<Key key="Return">select</Key>
	<Key key="Escape">escape</Key>
	<Key mask="A" key="Tab">nextstacked</Key>
	<Key mask="A" key="F4">close</Key>
	<Key mask="A" key="space">window</Key>
	<Key mask="A" key="F10">maximize</Key>
	<Key mask="CA" key="D">showdesktop</Key>
	<Key mask="CAS" key="R">exec:jwm -restart</Key>
	<!-- TO USE SUPER kEY /WINDOWS KEY TO SUMMON THE ROOT MENU. "133" IN MY KEYBOARD IS SUPER KEY. RUN XEV AND PRESS THE SUPER KEY AND USE THE KEY CODE THAT THE TERMINAL WINDOW SHOWS -->
	<Key keycode="133">root:1</Key>
	<!-- TO USE Print screen key to get a screen shot. IF "111" or "107" DON'T NOT WORK FOR YOU, RUN XEV AND PRESS THE SUPER KEY AND USE THE KEY CODE THAT THE TERMINAL WINDOW SHOWS -->
	<Key keycode="107">exec:antixscreenshot.sh</Key>
	
<!--THE FOLLOWING ALWAYS HAS TO BE THE LAST LINE!!!-->
</JWM>